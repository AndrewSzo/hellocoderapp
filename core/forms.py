from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from core.models import Tech, CustomUser, Roadmap


class TechForm(forms.ModelForm):
    class Meta:
        model = Tech
        fields = ("image", "title", "link", "desc")


class RoadmapForm(forms.ModelForm):
    class Meta:
        model = Roadmap
        fields = ("image", "title", "desc")


class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = CustomUser
        fields = ("username", "email", "password")


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = ("username", "email")