# Generated by Django 3.2 on 2021-06-09 09:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_alter_tech_image'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tech',
            old_name='name',
            new_name='title',
        ),
    ]
