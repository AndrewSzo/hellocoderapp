from django.shortcuts import render, redirect, get_object_or_404
from core.models import Tech, Roadmap, Tutorial
from django.views.generic.edit import CreateView
from core.forms import CustomUserCreationForm, TechForm
from django.contrib.auth.decorators import login_required


def home(request):
    queryset = Tech.objects.all()
    return render(request, 'dashboard/index.html', {'list_of_tech': queryset})


def login(request):
    return render(request, 'registration/login.html')


def logout(request):
    return render(request, 'dashboard/login.html')


def register(request):
    return render(request, 'dashboard/register.html')



def dashboard(request):
    queryset_one = Tech.objects.all()[0:6]

    queryset_two = Tech.objects.all()[7:12]
    queryset_roadmap_one = Roadmap.objects.all()[0:4]
    queryset_roadmap_two = Roadmap.objects.all()[5:8]
    return render(request, 'dashboard/index.html', {
        'list_of_tech_one': queryset_one,
        'list_of_tech_two': queryset_two,
        'list_of_roadmaps_one': queryset_roadmap_one,
        'list_of_raodmaps_two': queryset_roadmap_two

    })


def tech(request):
    queryset = Tech.objects.all()
    find_by_text_query = request.GET.get('find_by_text')
    if find_by_text_query != '' and find_by_text_query is not None:
        queryset = queryset.filter(name__icontains=find_by_text_query)
    return render(request, 'dashboard/pages/page-technologie.html', {'list_of_tech': queryset})


def tech_detail(request, title):
    tech = get_object_or_404(Tech, title=title)
    return render(request, 'dashboard/pages/page-technologie-details.html', {'tech':tech})

def tech_create(request, title):
    form = TechForm(request.POST or None)
    if form.is_valid():
        pass
    tech = get_object_or_404(Tech, title=title)
    return render(request, 'dashboard/pages/page-technologie-details.html', {'tech':tech})

@login_required(login_url='/accounts/login/')
def tutorials(request):
    queryset = Tutorial.objects.all()
    return render(request, 'dashboard/pages/page-technologie-details.html', {'list_of_tutorials': queryset})


def roadmaps(request):
    queryset = Roadmap.objects.all()
    find_by_text_query = request.GET.get('find_by_text')
    if find_by_text_query != '' and find_by_text_query is not None:
        queryset = queryset.filter(title__icontains=find_by_text_query)
    return render(request, 'dashboard/pages/page-roadmaps.html', {'list_of_roadmaps': queryset } )


def roadmap_detail(request, title):
    roadmap=get_object_or_404(Roadmap, title=title)
    return render(request, 'dashboard/pages/page-roadmaps-detail.html', {'roadmap':roadmap})


def skillset(request):
    return render(request, 'dashboard/pages/skillsets.html')


def devskillset(request):
    return render(request, 'dashboard/pages/devskillset.html')


def testskillset(request):
    return render(request, 'dashboard/pages/testskillset.html')

@login_required(login_url='/accounts/login/')
def devopsskillset(request):
    return render(request, 'dashboard/pages/devopsskillset.html')


def myresources(request):
    return render(request, 'dashboard/pages/page-user-resources.html')


def settings(request):
    return render(request, 'dashboard/pages/page-settings.html')


def academy(request):
    return render(request, 'dashboard/pages/academy.html')


def subskrypcja(request):
    return render(request, 'dashboard/pages/subskrypcja.html')


def chat(request):
    return render(request, 'dashboard/pages/chat.html')

def userprofile(request):
    return render(request, 'dashboard/pages/page-user-profile.html')

def feedback(request):
    return render(request, 'dashboard/pages/page-feedback.html')
