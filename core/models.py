from django.db import models
from django.urls import reverse
import os

from django.contrib.auth.models import AbstractUser

# Create your models here.


class CustomUser(AbstractUser):
    pass


class Tech(models.Model):
    def upload_path(self, filename):
        return os.path.join('tech/', filename)

    TYPES = (
        ('default', '---'),
        ('kurs', 'Kurs'),
        ('video', 'Wideo'),
        ('article', 'Artykuł'),
    )

    LEVELS = (
        ('default', '---'),
        ('beginer', 'Początkujący'),
        ('middle', 'Średniozaawansowany'),
        ('advance', 'Zaawansowany'),
    )

    SECTIONS = (
        ('default', '---'),
        ('programming', 'Programowanie'),
        ('testing', 'Testowanie'),
        ('devops', 'DevOps'),
    )

    image = models.FileField(upload_to=upload_path, default='tech/{{name}}')
    card_num = models.CharField(max_length=20, default='')
    title = models.CharField(max_length=500, default='')
    link = models.URLField(max_length=200, default='')
    desc = models.TextField(max_length=500, default='')
    type = models.CharField(max_length=32, choices=TYPES, default='default')
    level = models.CharField(max_length=32, choices=LEVELS, default='default')
    section = models.CharField(max_length=32, choices=SECTIONS, default='default')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('technologieDetails', args=[self.title])


class Roadmap(models.Model):
    def upload_path(self, filename):
        return os.path.join('roadmap/', filename)

    image = models.ImageField(upload_to=upload_path, default='roadmap/{{name}}')
    title = models.CharField(max_length=500, default='')
    desc = models.TextField(max_length=500, default='')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('roadmapDetails', args=[self.title])


class Tutorial(models.Model):

    title = models.CharField(max_length=500, default='')
    link = models.TextField(max_length=500, default='')
    desc = models.TextField(max_length=500, default='')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('tutorialDetails', args=[self.title])