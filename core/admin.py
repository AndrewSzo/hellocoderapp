from django.contrib import admin
from core.models import Tech, CustomUser, Roadmap, Tutorial
from core.forms import CustomUserCreationForm, CustomUserChangeForm


class CustomUserAdmin(admin.ModelAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    list_display = ['username', 'email']

# Register your models here.


admin.site.register(Tech)
admin.site.register(Roadmap)
admin.site.register(Tutorial)
admin.site.register(CustomUser, CustomUserAdmin)
