from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from core import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name='home'),
    path('accounts/', include('allauth.urls')),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('technologie/', views.tech, name='technologie'),
    path('technologie/<str:title>/', views.tech_detail, name='technologieDetails'),
    path('skillsets/', views.skillset, name='skillset'),
    path('roadmaps/', views.roadmaps, name='roadmaps'),
    path('roadmaps/<str:title>/', views.roadmap_detail, name='roadmapDetails'),
    path('developer-skillset/', views.devskillset, name='devskillset'),
    path('tester-skillset/', views.testskillset, name='testskillset'),
    path('devops-skillset/', views.devopsskillset, name='devopsskillset'),
    path('resources/', views.myresources, name='myresources'),
    path('settings/', views.settings, name='settings'),
    path('academy/', views.academy, name='academy'),
    path('subskrypcja/', views.subskrypcja, name='subskrypcja'),
    path('chat/', views.chat, name='chat'),
    path('profile/', views.userprofile, name='userprofile'),
    path('feedback/', views.feedback, name='feedback'),

]

if settings.DEBUG:
     urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)